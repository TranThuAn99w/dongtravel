var navbar = $('#navbar');

$(window).scroll(function () {
    if ($(window).scrollTop() > 0) {
        navbar.addClass('active');

    } else {
        navbar.removeClass('active');
    }

    if ($(window).scrollTop() > 200) {
        $('#back-to-top').addClass('active');
    } else {
        $('#back-to-top').removeClass('active');
    }

});

$("#menu-click").click(function () {
    $("#myTopnav").toggleClass("responsive");
});

// $('.owl-carousel').owlCarousel({
//   margin: 15,
//   nav: true,
// //   navText: ["<div class='nav-button owl-prev'>‹</div>", "<div class='nav-button owl-next'>›</div>"],
//   responsive: {
//     0: {
//       items: 1
//     },
//     600: {
//       items: 2
//     },
//     1000: {
//       items: 6
//     }
//   }
// });

$('#advantage-slide').owlCarousel({
    loop: false,
    autoplay: true,
    autoWidth: false,
    margin: 0,
    nav: true,
    dots: false,
    responsive: {
        0: {
            items: 1
        },
        600: {
            items: 2
        },
        1000: {
            items: 3
        }
    }
})

$('#cam-nghi-slide11').owlCarousel({
    loop: true,
    // autoplay: true,
    margin: 0,
    nav: true,
    navText: [
        "<div class='nav-btn prev-slide'><i class='fas fa-chevron-left'></i></div>",
        "<div class='nav-btn next-slide'><i class='fas fa-chevron-right'></i></div>"
    ],
    dots: false,
    responsive: {
        0: {
            items: 3
        },
        600: {
            items: 3
        },
        1000: {
            items: 5
        },
        1024: {
            items: 6
        }
    }
});

$('#dich-vu-slide').owlCarousel({
    loop: true,
    autoplay: true,
    // autoWidth:true,
    margin: 15,
    nav: true,
    items: 4,
    navText: [
        "<div class='nav-btn prev-slide'><i class='fas fa-chevron-left'></i></div>",
        "<div class='nav-btn next-slide'><i class='fas fa-chevron-right'></i></div>"
    ],
    dots: false,
    responsive: {
        0: {
            items: 2
        },
        600: {
            items: 3
        },
        1000: {
            items: 4
        }
    }
});

$('#tour-con').owlCarousel({
    loop: true,
    autoplay: true,
    autoplayTimeout: 2000,
    autoplayHoverPause: true,
    // autoWidth:true,
    margin: 15,
    nav: true,
    dots: false,
    responsive: {
        0: {
            items: 2
        },
        600: {
            items: 3
        },
        1000: {
            items: 6
        }
    }
})

var inputs = document.querySelectorAll(".product-list .wrap-item .custom-btn");

for (i = 0; i < inputs.length; i++) {
    inputs[i].addEventListener('click', function () {
        var price = this.querySelector('.money') ? this.querySelector('.money').innerHTML : '';
        var name = this.querySelector('h4') ? this.querySelector('h4').innerHTML : '';
        var cateName = document.querySelector('h1').innerHTML;

        console.log(name);
        console.log(price);
        console.log(cateName);

        window.dataLayer = window.dataLayer || [];
        window.dataLayer.push({
            event: 'eec.impressionClick',
            ecommerce: {
                click: {
                    actionField: {
                        list: cateName
                    },
                    products: [{ //  adding a product to a shopping cart.
                        'name': name,
                        'price': price,
                    }]

                }
            }
        });
    });
}