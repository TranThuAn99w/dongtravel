$ = jQuery;

$('#banner-slide').owlCarousel({
    loop: true,
    autoplay: true,
    autoWidth: false,
    margin: 0,
    nav: false,
    dots: false,
    responsive: {
        0: {
            items: 1
        },
        600: {
            items: 1
        },
        1000: {
            items: 1
        }
    }
})

$('#advantage-slide').owlCarousel({
    loop: false,
    autoplay: true,
    autoWidth: false,
    margin: 0,
    nav: true,
    dots: false,
    responsive: {
        0: {
            items: 1
        },
        600: {
            items: 2
        },
        1000: {
            items: 3
        }
    }
})

$('#service-slide').owlCarousel({
    loop: false,
    autoplay: true,
    autoWidth: false,
    margin: 0,
    nav: true,
    dots: false,
    responsive: {
        0: {
            items: 2
        },
        600: {
            items: 2
        },
        1000: {
            items: 2
        }
    }
})


$('#hot-slide').owlCarousel({
    loop: true,
    autoplay: true,
    autoWidth: true,
    margin: 0,
    nav: true,
    dots: false,
    items: 1,
    navText: [
        "<div class='nav-btn prev-slide'><i class='fas fa-chevron-left'></i></div>",
        "<div class='nav-btn next-slide'><i class='fas fa-chevron-right'></i></div>"
    ],
})

$('#location-slide').owlCarousel({
    loop: true,
    // autoplay: true,
    // autoWidth:true,
    margin: 15,
    nav: true,
    navText: [
        "<div class='nav-btn prev-slide'><i class='fas fa-chevron-left'></i></div>",
        "<div class='nav-btn next-slide'><i class='fas fa-chevron-right'></i></div>"
    ],
    dots: false,
    responsive: {
        0: {
            items: 2
        },
        600: {
            items: 3
        },
        1000: {
            items: 5
        }
    },

})

$('#ngoai-nuoc-slide').owlCarousel({
    loop: true,
    autoplay: true,
    autoplayTimeout: 2000,
    autoplayHoverPause: true,
    // autoWidth:true,
    margin: 15,
    nav: true,
    dots: false,
    navText: [
        "<div class='nav-btn prev-slide'><i class='fas fa-chevron-left'></i></div>",
        "<div class='nav-btn next-slide'><i class='fas fa-chevron-right'></i></div>"
    ],
    responsive: {
        0: {
            items: 2
        },
        600: {
            items: 2
        },
        1000: {
            items: 4
        }
    }
})

$('#trong-nuoc-slide').owlCarousel({
    loop: true,
    autoplay: true,
    autoplayTimeout: 2000,
    autoplayHoverPause: true,
    // autoWidth:true,
    margin: 15,
    nav: true,
    dots: false,
    navText: [
        "<div class='nav-btn prev-slide'><i class='fas fa-chevron-left'></i></div>",
        "<div class='nav-btn next-slide'><i class='fas fa-chevron-right'></i></div>"
    ],
    responsive: {
        0: {
            items: 2
        },
        600: {
            items: 2
        },
        1000: {
            items: 4
        }
    }
})

$('#dich-vu-slide').owlCarousel({
    loop: true,
    autoplay: true,
    // autoWidth:true,
    margin: 15,
    nav: true,
    navText: [
        "<div class='nav-btn prev-slide'><i class='fas fa-chevron-left'></i></div>",
        "<div class='nav-btn next-slide'><i class='fas fa-chevron-right'></i></div>"
    ],
    dots: false,
    responsive: {
        0: {
            items: 2
        },
        600: {
            items: 3
        },
        1000: {
            items: 4
        }
    }
})

$('#am-thuc-slide').owlCarousel({
    loop: true,
    autoplay: true,
    // autoWidth:true,
    margin: 15,
    nav: true,
    navText: [
        "<div class='nav-btn prev-slide'><i class='fas fa-chevron-left'></i></div>",
        "<div class='nav-btn next-slide'><i class='fas fa-chevron-right'></i></div>"
    ],
    dots: false,
    responsive: {
        0: {
            items: 2
        },
        600: {
            items: 3
        },
        1000: {
            items: 5
        }
    }
})

$('#cam-hung-slide').owlCarousel({
    loop: true,
    autoplay: true,
    margin: 0,
    nav: true,
    navText: [
        "<div class='nav-btn prev-slide'><i class='fas fa-chevron-left'></i></div>",
        "<div class='nav-btn next-slide'><i class='fas fa-chevron-right'></i></div>"
    ],
    dots: false,
    items: 1
})

$('#cam-nghi-slide').owlCarousel({
    loop: true,
    autoplay: true,
    margin: 20,
    nav: true,
    navText: [
        "<div class='nav-btn prev-slide'><i class='fas fa-chevron-left'></i></div>",
        "<div class='nav-btn next-slide'><i class='fas fa-chevron-right'></i></div>"
    ],
    dots: true,
    responsive: {
        0: {
            items: 1
        },
        600: {
            items: 1
        },
        1000: {
            items: 1
        }
    }
})

$('#gioi-thieu-slide').owlCarousel({
    loop: true,
    autoplay: true,
    margin: 20,
    nav: true,
    navText: [
        "<div class='nav-btn prev-slide'><i class='fas fa-chevron-left'></i></div>",
        "<div class='nav-btn next-slide'><i class='fas fa-chevron-right'></i></div>"
    ],
    dots: false,
    responsive: {
        0: {
            items: 1
        },
        600: {
            items: 3
        },
        1000: {
            items: 4
        }
    }
})
$('#testimonial-carousel').owlCarousel({
    loop: true,
    autoplay: true,
    margin: 20,
    // nav: none,
    navText: [
        "<div class='nav-btn prev-slide'><i class='fas fa-chevron-left'></i></div>",
        "<div class='nav-btn next-slide'><i class='fas fa-chevron-right'></i></div>"
    ],
    dots: false,
    responsive: {
        1000: {
            items: 1
        }
    }
})




$('#doi-tac-slide').owlCarousel({
    loop: true,
    autoplay: true,
    // autoWidth:true,
    margin: 15,
    nav: false,
    dots: false,
    responsive: {
        0: {
            items: 2
        },
        600: {
            items: 4
        },
        1000: {
            items: 8
        }
    }
})

$("#menu-click").click(function() {
    $("#myTopnav").toggleClass("responsive");
});



//Accordion Hover
var acc = document.getElementsByClassName("accordion");
var i;
var a;

for (i = 0; i < acc.length; i++) {
    acc[0].classList.add('active');

    acc[i].addEventListener("mouseenter", function() {
        // this.classList.toggle("active");

        for (a = 0; a < acc.length; a++) {
            acc[a].classList.remove('active');
        }

        this.classList.add('active');
        // var panel = this.nextElementSibling;
        // if (panel.style.maxHeight) {
        //     panel.style.maxHeight = null;
        // } else {
        //     panel.style.maxHeight = panel.scrollHeight + "px";
        // }

    });
}

var search = document.getElementById('banner-search');
var search_wrap = document.getElementById('search-wrap');

var result = document.getElementById('search-result');
var result_mobile = document.getElementById('search-mobile');
search.addEventListener("click", function() {
    if (window.innerWidth > 767) {
        result_mobile.style.display = 'none';
        result.style.display = 'block';
    } else {
        result.style.display = 'none';
        result_mobile.style.display = 'block';
    }
});


$(document).click(function(event) {
    //if you click on anything except the modal itself or the "open modal" link, close the modal
    if (result && !$(event.target).closest(".banner-search, .search-result").length) {
        result.style.display = "none";
    };
    if (result_mobile && !$(event.target).closest(".banner-search, .search-mobile").length) {
        result_mobile.style.display = "none";
    };
});


function openCity(evt, cityName) {
    var i, tabcontent, tablinks;
    tabcontent = document.getElementsByClassName("tabcontent");
    for (i = 0; i < tabcontent.length; i++) {
        tabcontent[i].style.display = "none";
    }
    tablinks = document.getElementsByClassName("tablinks");
    for (i = 0; i < tablinks.length; i++) {
        tablinks[i].className = tablinks[i].className.replace(" active", "");
    }
    document.getElementById(cityName).style.display = "block";
    evt.currentTarget.className += " active";
}

$(document).keyup(function(e) {
    // if (e.keyCode === 13) result.style.display = 'none';;     // enter
    if (e.keyCode === 27) result_mobile.style.display = 'none'; // esc
});