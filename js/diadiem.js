$ = jQuery;

var navbar = $('#navbar');

$(window).scroll(function () {
    if ($(window).scrollTop() > 0) {
        navbar.addClass('active');

    } else {
        navbar.removeClass('active');
    }

    if ($(window).scrollTop() > 200) {
        $('#back-to-top').addClass('active');
    } else {
        $('#back-to-top').removeClass('active');
    }

});

const postDetails = document.querySelector(".section");

(function ($) {
    "use strict"; // Start of use strict

    // Smooth scrolling using jQuery easing
    $('a.js-scroll-trigger[href*="#"]:not([href="#"])').click(function () {
        if (location.pathname.replace(/^\//, '') == this.pathname.replace(/^\//, '') && location.hostname == this.hostname) {
            var target = $(this.hash);
            target = target.length ? target : $('[name=' + this.hash.slice(1) + ']');
            if (target.length) {
                $('html, body').animate({
                    scrollTop: (target.offset().top - 70)
                }, 500, "easeInOutExpo");
                return false;
            }
        }
    });

    // faq
    var acc = document.getElementsByClassName("faq");
    var i;

    for (i = 0; i < acc.length; i++) {
        acc[i].addEventListener("click", function () {
            this.classList.toggle("active");
            var panel = this.nextElementSibling;
            if (panel.style.maxHeight) {
                panel.style.maxHeight = null;
            } else {
                panel.style.maxHeight = panel.scrollHeight + "px";
            }
        });
    }

    // Closes responsive menu when a scroll trigger link is clicked
    // $('.js-scroll-trigger').click(function() {
    //     $('.navbar-collapse').collapse('hide');
    // });

    // Activate scrollspy to add active class to navbar items on scroll
    // $('body').scrollspy({
    //     target: '#toc'
    // });

})(jQuery); // End of use strict